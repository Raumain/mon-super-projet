FROM httpd
FROM php:7.4-apache

RUN apt-get update 
RUN apt-get install -y libzip-dev zip unzip 
RUN docker-php-ext-install mysqli pdo pdo_mysql 
RUN a2enmod rewrite

COPY . /var/www/html

WORKDIR /var/www/html

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === 'edb40769019ccf227279e3bdd1f5b2e9950eb000c3233ee85148944e555d97be3ea4f40c3c2fe73b22f875385f6a5155') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');"

ADD src /usr/local/apache2/htdocs
EXPOSE  80

CMD ["apachectl", "-D", "FOREGROUND"]
